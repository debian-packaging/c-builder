# Copyright 2019-2025 IQRF Tech s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM iqrftech/debian-base-builder:ubuntu-noble-armhf

LABEL maintainer="Roman Ondráček <roman.ondracek@iqrf.com>"

ENV DEBIAN_FRONTEND="noninteractive"

RUN apt-get update \
  && apt-get install --no-install-recommends -t noble-backports -y \
     ccache cmake cppcheck doxygen gcovr googletest graphviz \
     libasio-dev libboost-filesystem-dev libboost-log-dev \
     libboost-program-options-dev libboost-system-dev libbz2-dev \
     libcurl4-openssl-dev libfmt-dev libgmock-dev libgpiod-dev libgtest-dev \
     libpaho-mqtt-dev libsqlite3-dev libsystemd-dev libssl-dev libwww-perl \
     libzip-dev libzmq3-dev pkg-config python3-requests zlib1g-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
